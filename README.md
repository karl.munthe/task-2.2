# Docker task  
  
## Requirements  
  
Docker  
  
## Usage  
  
docker build. -t cowsay  
docker run --rm cowsay  
  
## Unique usage  
  
You can use for example want to use some coding program like R or Haskell without downloading them you can use them through Docker
  
## Maintainers  
  
[Karl Munthe](https://gitlab.com/karl.munthe)